-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Sep 2020 pada 05.09
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crud_siswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kec` int(1) NOT NULL,
  `id_kota` int(1) NOT NULL,
  `nama_kec` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id_kec`, `id_kota`, `nama_kec`) VALUES
(1, 1, 'Bandung Timur'),
(2, 2, 'Cimahi Utara'),
(3, 3, 'Padalarang'),
(4, 2, 'Cimahi Tengah'),
(5, 1, 'Antapani'),
(6, 3, 'Lembang'),
(7, 2, 'Cimahi Selatan'),
(8, 3, 'Batujajar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(1) NOT NULL,
  `nama_kota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_kota`) VALUES
(1, 'Kota Bandung'),
(2, 'Kota Cimahi'),
(3, 'Kab. Bandung Barat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL,
  `siswa_nama` varchar(255) NOT NULL,
  `siswa_kota` int(11) NOT NULL,
  `siswa_kec` int(11) NOT NULL,
  `siswa_alamat` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`siswa_id`, `siswa_nama`, `siswa_kota`, `siswa_kec`, `siswa_alamat`, `created_on`) VALUES
(1, 'Agus', 1, 1, 'Alamat Siswa', '2020-09-14 12:46:27'),
(2, 'Budi', 2, 2, 'Alamat Siswa', '2020-08-29 12:46:37'),
(3, 'Nana', 3, 3, 'Alamat Siswa', '2020-08-29 12:46:48'),
(4, 'Bambang', 3, 3, 'Alamat Siswa', '2020-08-29 12:47:11'),
(5, 'Fitri', 2, 4, 'Alamat Siswa', '2020-08-29 12:47:27'),
(6, 'Bagus', 1, 5, 'Alamat Siswa', '2020-08-29 12:47:38'),
(7, 'Hartoko', 1, 5, 'Alamat Siswa', '2020-08-29 12:47:51'),
(8, 'Dadan', 3, 6, 'Alamat Siswa', '2020-08-29 12:48:05'),
(9, 'Ceceng', 2, 7, 'Alamat Siswa', '2020-08-29 12:48:22'),
(10, 'Ilham', 1, 1, 'Alamat Siswa', '2020-08-29 12:48:35'),
(11, 'Iqbal', 3, 8, 'Alamat Siswa', '2020-08-29 12:48:50'),
(12, 'Adi', 2, 4, 'Alamat Siswa', '2020-08-29 12:49:02');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indeks untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`siswa_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kec` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `siswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
