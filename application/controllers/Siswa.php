<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
    * Tes HumanUp Indonesia
    * @author Ridho Habi Putra
    * 2020
    */

    class Siswa extends CI_Controller
    {

        function __construct()
        {
            parent::__construct();
        }

        public function index()
        {
        	$data['title'] = 'Daftar Siswa';
        	$data['siswa'] = $this->siswa_model->daftar_siswa();

        	$this->load->view('daftar_siswa', $data);
        }

        public function registrasi()
        {

            $data['title'] = 'Data Siswa';
            $get_kota = $this->db->select('*')->from('kota')->get();
            $get_kec = $this->db->select('*')->from('kecamatan')->get();

            $data['kec']  = $get_kec->result();
            $data['kota'] = $get_kota->result();
            $data['path'] = base_url('assets');

            $this->form_validation->set_rules('nama', 'Nama', 'required');
        
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('input_siswa', $data);
            
            }else{

            	$this->siswa_model->input_siswa();
            	$this->session->set_flashdata('success', 'Input Data Siswa.');
                redirect('siswa/');
            }
        }

        public function edit($id)
        {	
        	$data['siswa'] = $this->siswa_model->detail_siswa($id);
        	$data['title'] = 'Update Data Siswa';

            $get_kota = $this->db->select('*')->from('kota')->get();
            $get_kec = $this->db->select('*')->from('kecamatan')->get();

            $data['kec']  = $get_kec->result();
            $data['kota'] = $get_kota->result();
            $data['path'] = base_url('assets');

        	$this->form_validation->set_rules('nama', 'Nama', 'required');
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('update_siswa', $data);
            
            }else{

            	$this->siswa_model->update_siswa($id);
            	$this->session->set_flashdata('success', 'Update Data Siswa.');
                redirect('siswa/');
            }
        }

        public function hapus($id)
        {
        	$this->siswa_model->delete_siswa($id);
            $this->session->set_flashdata('success', 'Hapus Data Siswa.');
               redirect('siswa/');
        }
      
        function kecamatan($id_kota)
        {
            $query = $this->db->get_where('kecamatan',array('id_kota'=>$id_kota));
            $data = "<option value=''> - Pilih Kecamatan - </option>";
            foreach ($query->result() as $value) {
                $data .= "<option value='".$value->id_kec."'>".$value->nama_kec."</option>";
            }
            echo $data;
        }
    }