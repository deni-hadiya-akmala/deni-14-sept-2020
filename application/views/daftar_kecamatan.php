<!DOCTYPE html>
<html>
<head>
<style>
#siswa {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#siswa td, #siswa th {
  border: 1px solid #ddd;
  padding: 8px;
}

#siswa tr:nth-child(even){background-color: #f2f2f2;}

#siswa tr:hover {background-color: #ddd;}

#siswa th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  color: black;
}

a {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}

.success {
  padding: 20px;
  background-color: #33cc33;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

</style>
</head>
<body>
<div class="topnav">
  <a href="<?php echo site_url();?>siswa">Daftar Siswa</a>
  <a href="<?php echo site_url();?>wilayah/kota">Daftar Kota/Kabupaten</a>
  <a href="<?php echo site_url();?>wilayah/kecamatan">Daftar Kecamatan</a>
</div>
<h1 style="text-align: center;">Daftar Kecamatan</h1>
<a href="<?php echo site_url();?>wilayah/input_kecamatan">Input Data Kecamatan</a><br><br>
<?php if($this->session->flashdata('success')): ?>
  <div class="success">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <strong>Berhasil</strong> <?php echo $this->session->flashdata('success'); ?>
  </div>
<?php elseif($this->session->flashdata('failed')): ?>
  <div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <strong>Berhasil</strong> <?php echo $this->session->flashdata('failed'); ?>
  </div>      
<?php endif; ?>
<table id="siswa">
  <tr>
    <th>Id Kecamatan</th>
    <th>Kecamatan</th>
    <th>Kota/Kabupaten</th>
    <th>Aksi</th>
  </tr>
  <?php foreach($kecamatan as $s) : ?>
    <tr>
      <td><?php echo $s['id_kec']; ?></td>
      <td><?php echo $s['nama_kec']; ?></td>
      <td><?php echo $s['nama_kota']; ?></td>
      <td>
        <a type="button" href="<?php echo site_url(); ?>wilayah/edit_kecamatan/<?php echo $s['id_kec']; ?>">Edit</a>
        <a style="background-color: #e31212; " type="button" href="<?php echo site_url(); ?>wilayah/hapus_kecamatan/<?php echo $s['id_kec']; ?>" onClick="return confirm('Hapus Data Kecamatan ?')">Hapus</a>
      </td>
    </tr>
  <?php endforeach ?>
</table>

</body>
</html>
