<!DOCTYPE html>
<html>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

textarea {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  
  background-color: #03bafc;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #03cefc;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

h1 {
  text-align: center;
}
</style>
<body>

<h1>Input Kecamatan</h1>

<div>
  <?php echo form_open('wilayah/input_kecamatan'); ?>
    <label for="fname">Nama Kecamatan</label>
    <input type="text" name="nama" placeholder="Nama Kecamatan">

    <select name="id_kota">
      <option value="">Pilih Kota/Kabupaten</option>
      <?php foreach($kota as $c): ?>
        <option value="<?php echo $c['id_kota']; ?>"><?php echo $c['nama_kota']; ?></option>
      <?php endforeach; ?>
    </select>

    <input type="submit" value="Submit">
  <?php echo form_close(); ?>
</div>

</body>
</html>
