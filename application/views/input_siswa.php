<!DOCTYPE html>
<html>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

textarea {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  
  background-color: #03bafc;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #03cefc;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

h1 {
  text-align: center;
}
</style>
<body>

<h1>Input Kota</h1>

<div>
  <?php echo form_open('siswa/registrasi'); ?>
    <label for="fname">Nama Siswa</label>
    <input type="text" name="nama" placeholder="Nama Siswa">

    <label for="kota">Kota/Kabupaten</label>
    <select id="kota" name="kota">
      <option>- Kota/Kabupaten -</option>
      <?php 
          foreach($kota as $k)
          {
            echo '<option value="'.$k->id_kota.'">'.$k->nama_kota.'</option>';
          }
      ?>
    </select>

    <label for="country">Kecamatan</label>
    <select name="kec" id="kecamatan">
      <option value=''>- Pilih Kecamatan -</option>
    </select>
  
    <label for="fname">Alamat</label>
    <textarea placeholder="Alamat" rows="5" name="alamat"></textarea>

    <input type="submit" value="Submit">
  <?php echo form_close(); ?>
</div>
<script src="<?php echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.min.js"></script>
<script>
    $(document).ready(function(){
        $("#kota").change(function (){
            var url = "<?php echo site_url('siswa/kecamatan');?>/"+$(this).val();
            $('#kecamatan').load(url);
            return false;
        })
    });
</script>

</body>
</html>
