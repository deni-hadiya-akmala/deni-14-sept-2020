<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
	/**
    * Tes HumanUp Indonesia
    * @author Ridho Habi Putra
    * 2020
    */

	class Siswa_model extends CI_Model
	{
		
		function __construct()
		{
			$this->load->database();
		}

		public function daftar_siswa()
		{
			$this->db->join('kota', 'kota.id_kota = siswa.siswa_kota');
			$this->db->join('kecamatan', 'kecamatan.id_kec = siswa.siswa_kec');
			$this->db->order_by('siswa.siswa_id', 'ASC');
			$query = $this->db->get('siswa');
			return $query->result_array();
		}

		public function input_siswa()
		{
			$data = array(
				'siswa_nama'		=> $this->input->post('nama'),
				'siswa_kota'		=> $this->input->post('kota'),
				'siswa_kec'			=> $this->input->post('kec'),
				'siswa_alamat'		=> mb_convert_case($this->input->post('alamat'), MB_CASE_TITLE, "UTF-8")
			);
			
			return $this->db->insert('siswa', $data);
		}

		public function detail_siswa($id)
		{
			$this->db->join('kota', 'kota.id_kota = siswa.siswa_kota');
			$this->db->join('kecamatan', 'kecamatan.id_kec = siswa.siswa_kec');
			$this->db->where('siswa_id', $id);
			$query = $this->db->get('siswa');
			return $query->row_array();
		}

		public function update_siswa($id)
		{
			$data = array(
				'siswa_nama'		=> $this->input->post('nama'),
				'siswa_kota'		=> $this->input->post('kota'),
				'siswa_kec'			=> $this->input->post('kec'),
				'siswa_alamat'		=> mb_convert_case($this->input->post('alamat'), MB_CASE_TITLE, "UTF-8")
			);

			$this->db->where('siswa_id', $id);
			return $this->db->update('siswa', $data);
		}

		public function delete_siswa($id)
		{        				
			$this->db->where('siswa_id', $id);
			$this->db->delete('siswa');
			return true;
		}
	}