<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
	/**
    * Tes HumanUp Indonesia
    * @author Ridho Habi Putra
    * 2020
    */

	class Wilayah_model extends CI_Model
	{
		
		function __construct()
		{
			$this->load->database();
		}

		public function daftar_kota()
		{
			$this->db->order_by('kota.id_kota', 'ASC');
			$query = $this->db->get('kota');
			return $query->result_array();
		}

		public function daftar_kecamatan()
		{
			$this->db->join('kota', 'kota.id_kota = kecamatan.id_kota');
			$this->db->order_by('kecamatan.id_kec', 'ASC');
			$query = $this->db->get('kecamatan');
			return $query->result_array();
		}

		public function input_kota()
		{
			$data = array(
				'nama_kota'			=> $this->input->post('nama')
			);
			
			return $this->db->insert('kota', $data);
		}

		public function input_kecamatan()
		{
			$data = array(
				'nama_kec'			=> $this->input->post('nama'),
				'id_kota'			=> $this->input->post('id_kota')
			);
			
			return $this->db->insert('kecamatan', $data);
		}

		public function detail_kota($id)
		{
			$this->db->where('id_kota', $id);
			$query = $this->db->get('kota');
			return $query->row_array();
		}

		public function detail_kecamatan($id)
		{
			$this->db->join('kota', 'kota.id_kota = kecamatan.id_kota');
			$this->db->where('id_kec', $id);
			$query = $this->db->get('kecamatan');
			return $query->row_array();
		}

		public function update_kota($id)
		{
			$data = array(
				'nama_kota'			=> $this->input->post('nama')
			);

			$this->db->where('id_kota', $id);
			return $this->db->update('kota', $data);
		}

		public function update_kecamatan($id)
		{
			$data = array(
				'nama_kec'			=> $this->input->post('nama'),
				'id_kota'			=> $this->input->post('id_kota')
			);

			$this->db->where('id_kec', $id);
			return $this->db->update('kecamatan', $data);
		}

		public function delete_kota($id)
		{        				
			$this->db->where('id_kota', $id);
			$this->db->delete('kota');
			return true;
		}

		public function delete_kecamatan($id)
		{        				
			$this->db->where('id_kec', $id);
			$this->db->delete('kecamatan');
			return true;
		}
	}